//Load Express Library
var express = require("express");

// For more information on express, visit this page http://expressjs.com/en/4x/api.html

// Create an instance of the express application
var app = express();

//Configure the routes
// Check and match request
app.use(express.static(__dirname + "/public"));
//note there that the "/images" portion is simply to net off the /images portion of the resource (stated in the html) so that the directory specified can be "/pics" and it will look for the stated image (maxresdefault.jpg) inside it
app.use("/images", express.static(__dirname + "/pics"));

// Create my server by defining a port that the app will be listening to
var port = 3000;

// Bind the app to the port
app.listen(port, function(){
    console.log("Application started on port %d", port);
});

